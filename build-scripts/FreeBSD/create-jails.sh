#создание jail для poudriere

#!/bin/sh
SCRIPT_EXEC_DIR=$( cd "$( dirname "$0" )" && pwd )

. ${SCRIPT_EXEC_DIR}/ports-env.sh

sudo poudriere jail -c -j 93Ramd64  -v  9.3-RELEASE -a amd64
sudo poudriere jail -c -j 93Ri386   -v  9.3-RELEASE -a i386
sudo poudriere jail -c -j 103Ramd64 -v 10.3-RELEASE -a amd64
sudo poudriere jail -c -j 103Ri386  -v 10.3-RELEASE -a i386 

sudo poudriere ports -c -F -f none -M ${PORTSDIR} -p lcarddev


