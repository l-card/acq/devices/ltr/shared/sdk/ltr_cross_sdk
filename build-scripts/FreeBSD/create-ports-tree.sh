#скрипт создает с нуля дерево портов и их бд в указанной директории и скачивает текущую версию портов

#!/bin/sh
SCRIPT_EXEC_DIR=$( cd "$( dirname "$0" )" && pwd )
. ${SCRIPT_EXEC_DIR}/ports-env.sh

rm -rf ${PORTSDIR}
rm -rf ${PORT_DBDIR}

mkdir ${PORTSDIR}
mkdir ${PORT_DBDIR}

portsnap -d ${PORT_DBDIR} -p ${PORTSDIR} fetch extract


