#!/bin/sh

#сприпт подготавливает файлы портов, перенося их в директории портов и
#архивы с исходниками, используемые портами, которые помещает в distfiles портов

SCRIPT_EXEC_DIR=$( cd "$( dirname "$0" )" && pwd )

. ${SCRIPT_EXEC_DIR}/ports-env.sh

BUILD_DIR=${SCRIPT_EXEC_DIR}/build

# удаляем результаты предыдущей сборки
rm -rf ${BUILD_DIR}
mkdir ${BUILD_DIR}
cd  ${BUILD_DIR}


cmake -DCMAKE_BUILD_TYPE=Release -DFREEBSD_PORTS_DIR=${PORTSDIR} ${SCRIPT_EXEC_DIR}/../.. 

make ltrd-freebsd-src-prepare
make ltrapi-freebsd-src-prepare
make ltrctl-freebsd-src-prepare

make ltrd-freebsd-port-prepare
make ltrapi-freebsd-port-prepare
make ltrctl-freebsd-port-prepare



