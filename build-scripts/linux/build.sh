#!/bin/sh
#получаем директорию, где лежит файл скрипта
SCRIPT_EXEC_DIR=$( cd "$( dirname "$0" )" && pwd )
set -e


# скрипт для сборки под ОС Linux
# следует установить нижеперечисленные параметры в значения,
# требуемые для Вашего случая, после чего запустить скрипт.
#
# необходимо наличие cmake, make, gcc
# (остальные зависимости необходимы только при включении
#  определенных параметров и указанны в коментариях к ним)



# --------------  Параметры сборки  ---------------------
# Путь к корневой директории, куда будут сохранены результаты.
# В данном случае все файлы будут в поддиректории install
# Если нужно установить в систему, то нужно раскоментеривоить
# строку с INSTALL_ROOT_CMD
export DESTDIR=${SCRIPT_EXEC_DIR}/install
# Если для установки требуются права суперпользователя, то раскомментировать
# (в этом случае DESTDIR уже влиять не будет)
#INSTALL_ROOT_CMD=sudo
# путь установки отностиельно корневой директории
INSTALL_PREFIX=/usr
# тип сборки (Release или Debug)
BUILD_TYPE=Release

#--------------- Что именно собирать --------------------
#собирать ли примеры на С для ltrxxxapi
BUILD_EXAMPLES=ON
#собирать ltrd (требует наличия файлов разработки для libxml2)
LTR_BUILD_LTRD=ON
#собирать ли LtrManager (требуется Qt4)
LTR_BUILD_LTRMANAGER=ON
#собирать ли утилиты конфигурации/обновления прошивки LTR-EU
LTR_BUILD_TOOLS_LTREU=ON
#собирать ли утилиту для обновления прошивки AVR LTR-модулей
LTR_BUILD_TOOLS_LTRAVRUPDATE=ON
#собирать ли утилиту-пример для LTR210 (Требует Qt,MathGL2.1,fftw)
LTR_BUILD_TOOLS_LTR210_OSC=OFF


#--------------- Настройки LTRD ------------------------------
#поддержка USB (требует libusb или lubusbx)
LTRD_USB_ENABLED=ON
#поддержка работы в виде демона systemd (используется в Fedora, OpenSuse, Arch...)
# требует наличие файлов разработки для systemd
LTRD_SYSTEMD_ENABLED=OFF
#поддержка запуска в виде демона (без systemd) - требует libdaemon
LTRD_DAEMON_ENABLED=OFF
#вывод лога через syslog
LTRD_SYSLOG_ENABLED=ON
#вывод лога на stdout
LTRD_STDOUT_ENABLED=ON




# удаляем результаты предыдущей сборки
rm -rf build
# удаляем результаты сборки только, если путь к установке находится внутри текущего каталога,
# чтобы не удалить системные файлы при установке в корень
case $DESTDIR in
  ${SCRIPT_EXEC_DIR}/?*) rm -rf ${DESTDIR};;
esac


cd ${SCRIPT_EXEC_DIR}
mkdir build
cd build
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=${BUILD_TYPE} -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} \
  -DLTR_BUILD_EXAMPLES=${BUILD_EXAMPLES} -DLTR_BUILD_LTRD=${LTR_BUILD_LTRD} \
  -DLTR_BUILD_LTRMANAGER=${LTR_BUILD_LTRMANAGER} -DLTR_BUILD_TOOLS_LTREU=${LTR_BUILD_TOOLS_LTREU} \
  -DLTR_BUILD_TOOLS_LTRAVRUPDATE=${LTR_BUILD_TOOLS_LTRAVRUPDATE}  -DLTR_BUILD_TOOLS_LTR210_OSC=${LTR_BUILD_TOOLS_LTR210_OSC} \
  -DLTRD_USB_ENABLED=${LTRD_USB_ENABLED} -DLTRD_SYSTEMD_ENABLED=${LTRD_SYSTEMD_ENABLED} -DLTRD_DAEMON_ENABLED=${LTRD_DAEMON_ENABLED} \
  -DLTRD_SYSLOG_ENABLED=${LTRD_SYSLOG_ENABLED} -DLTRD_STDOUT_ENABLED=${LTRD_STDOUT_ENABLED} \
   ${SCRIPT_EXEC_DIR}/../.. && make && ${INSTALL_ROOT_CMD} make install

