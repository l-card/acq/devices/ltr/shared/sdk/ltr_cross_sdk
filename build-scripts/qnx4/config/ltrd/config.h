#ifndef CONFIG_H_IN
#define CONFIG_H_IN


/* версия ltrd */
#define LTRD_VERSION_MAJOR 2
#define LTRD_VERSION_MINOR 1
#define LTRD_VERSION_REV   4
#define LTRD_VERSION_PATCH 14


#define LTRD_PROGRAMM_NAME "ltrd"
#define LTRD_CRATE_NAME    "LTR"
#define LTR_MODULE_NAME    "LTR"

/* проверка наличия в системе нужных функций и файлов */

/* наличие библиотеки libdaemon */
/* #undef HAVE_LIBDAEMON */
/* наличие функции strlen */
/* #undef HAVE_STRNLEN */
/* наличие файла string.h */
#define HAVE_STRING_H 1
/* наличие libmxl2 */
#define HAVE_LIBXML2 1
/* наличие int64_t */
/* #undef HAVE_INT64 */
/* наличие uint64_t */
/* #undef HAVE_UINT64 */
/* наличие функции initgroups */
/* #undef HAVE_INITGROUPS */
/* наличие структуры sockaddr_in6 */
/* #undef HAVE_SOCKADDR_IN6 */
/* наличие типа socklen_t */
/* #undef HAVE_SOCKLEN_T */
/* наличие файла ifaddrs.h */
/* #undef HAVE_IFADDRS_H */
/* наличие sys/timers.h */
#define HAVE_SYS_TIMERS_H 1
/* наличие типа клока CLOCK_MONOTONIC */
/* #undef HAVE_CLOCK_MONOTONIC */
/* наличие libusb_error_name() при включенной поддержке libusb */
/* #undef HAVE_LIBUSB_ERROR_NAME */









#ifndef HAVE_STRLEN
    #ifdef HAVE_STRING_H
        #include <string.h>
    #endif
    size_t strnlen(const char *s, size_t maxlen);
#endif

#include "stdint.h"

#ifndef HAVE_UINT64

    typedef struct
    {
        uint32_t l;
        uint32_t h;
    } uint64_t;

#define UINT64_INC(val) \
    do {\
        if (val.l!=UINT32_MAX) \
            val.l = val.l + 1; \
        else \
        { \
            val.l = 0; \
            val.h = val.h+1; \
        } \
    } while (0)

#define UINT64_ADD32(val, add) \
    do {\
        uint32_t delta = (UINT32_MAX-val.l); \
        if (delta >= (uint32_t)add) \
            val.l = val.l + add; \
        else \
        { \
            val.l = add-delta-1; \
            val.h = val.h+1; \
        } \
    } while (0)

#define UINT64_ASSIGN32(dst, src) \
    do { \
        dst.l = src; \
        dst.h = 0; \
    } while(0)

#define UINT64_OR(val, high, low) \
    do { \
        (val).l |= (low); \
        (val).h |= (high); \
    } while(0)

#define UINT64_LOW(val)   val.l
#define UINT64_HIGH(val)  val.h

#define UINT64_IS_ZERO(val) ((val.l==0) && (val.h==0))



#else
    #define UINT64_INC(val)             val++
    #define UINT64_ADD32(val, add)      val+=add
    #define UINT64_ASSIGN32(dst, src)   dst = src
    #define UINT64_OR(val, h, l)        (val = val | (((uint64_t)(h)) << 32) | (uint64_t)(l))
    #define UINT64_LOW(val)             (val & 0xFFFFFFFF)
    #define UINT64_HIGH(val)            ((val >> 32) & 0xFFFFFFFF)
    #define UINT64_IS_ZERO(val)         (val==0)
#endif


/* Размер буфера сокета на прием данных из крейта */
#define LTRD_ETH_CRATE_SOCK_RECV_BUF_SIZE  (128*1024)
/* Размер буфера сокета на передачу данных в крейт */
#define LTRD_ETH_CRATE_SOCK_SEND_BUF_SIZE  (128*1024)

/* включение-выключение опций */

/* При разрешении этой опции включается код для запуска ltrd в режиме
 * демона (отключаеся от консоли, закрывает все хендлы и т.д.) при передаче
 * ключа --daemon */
/* #undef LTRD_DAEMON_ENABLED */

/* При включении этой опции разрешается вывод на stdout (если ltrd был запущен
   без ключа --daemon) */
#define LTRD_STDOUT_ENABLED

/* При включении этой опции разрешается вывод в системный лог либо
 * с помощью syslog() или через daemon_log() */
#define LTRD_SYSLOG_ENABLED

/* Включение поддержки режима работы с systemd */
/* #undef LTRD_SYSTEMD_ENABLED */

/* Включение кода рассчета статистики скорости передачи */
#define LTRD_BAND_STATISTIC_ENABLED

/* Включение поддержки USB */
/* #undef LTRD_USB_ENABLED */

/* Поддуржка API для получения лога сервера */
#define LTRD_LOGPROTO_ENABLED


/* Разрешение сохранения дампа передач при ошибке крейта в файл */
/* #undef LTRD_ETH_DUMP_ENABLED */



/* файл с прошивкой ПЛИС для LTR-U-8/16 (LTR010) */
#define LTRD_LTR010_FIRMWARE_FILE "/ltr010v3_0_5.ttf"

/* файл с конфигурацией по умолчанию */
#define LTRSRV_DEFAULT_CONFIG_FILE "/etc/ltrd/config.xml"

#endif // CONFIG_H_IN
