#ifndef CONFIG_H_IN
#define CONFIG_H_IN

#include "ltrapi_config.h"

/* проверка наличия в системе нужных функций и файлов */
/* #undef HAVE_STRNLEN */
#define HAVE_STRING_H 1
/* #undef HAVE_CLOCK_MONOTONIC */
/* #undef HAVE_SOCKLEN_T */
/* #undef HAVE_MSG_NOSIGNAL */
#define HAVE_SYS_TIMERS_H  1


#ifndef HAVE_STRNLEN
    #include <string.h>
    size_t strnlen(const char *s, size_t maxlen);
#endif


#endif // CONFIG_H_IN
