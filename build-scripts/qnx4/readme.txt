������ ltr ��� QNX4.

�������������� ������ ltr ��� QNX4 � Windows ����� � �������������� ����������� OpenWatcom 1.9 � cmake (��� ��������� makefile'��).

����������: �������� ������ � ��������������� � QNX4, �� ��� ����� ��������� ��������� ����� makefile'��. ��� ����� ������ ����� ������������ �� � ����� config ���������, ������������ ������������� � ������� cmake. 
��� ��������� makefile ��� QNX ����������, ����� ��� ���������� ��� ��������� ������ HAVE_CONFIG_H � ������� ����� -Wc,-ei ��� ���������� ltrd.

��� ������ ��� �� Windows �����:
1. ������� � ���������� ���������� OpenWatcom � ������������ ����� http://www.openwatcom.org/ (�������� http://openwatcom.mirrors.pair.com/open-watcom-c-win32-1.9.exe)
2. ����� ��������� OpenWatcom ���������� ������������� ���������
3. ���������� ����������� ��������� � ����������� QNX4 � �:/QNX4, ��� �������  ����� http://forum.kpda.ru/index.php?PHPSESSID=7qgddr8ishvka45q1fbns8bii2&/topic,269.msg2372.html#msg2372.
   ����� �������� ��������� �������� ��� ����� String.h � string.h �� ����� ������������ ������ � Windows (������� ��� ������ ����� ������ ���� ��� C - string.h)
4. ������� ������ ��������� OpenWatcom ������ http://www.kpda.ru/download=ow_q4_win_20081224.zip � ��������� setup.bat
5. ������� libxml2 ��� QNX (�������� ������: http://forum.kpda.ru/index.php/topic,261.0.html) � ��������� ��������� � <���� � Watcom>/h/qnx4, � .lib ���� � <���� � Watcom>/lib386/qnx4
6. ��� ��� getiffaddrs.c �� �����-�� �������� ��������� � ��������� ��� ��� OpenWatcom, ���������� ����� �� ltrd/lib/ifaddrs �������������� � ���������� ��������������� ��� QNX4 � ������� "cc -a getifaddrs -g -l socket3r -l clib3r getifaddrs.c" � �������� ������������ getifaddrs.lib � <���� � Watcom>/lib386/qnx4.
   ���� �� ������ ������� ��� ��������� ���������� ��� getifaddrs � http://cdn.bitbucket.org/lcard/ltr_cross_sdk/downloads/getifaddrs_qnx4.zip
7. ������� � ���������� cmake � http://cmake.org (http://www.cmake.org/files/v2.8/cmake-2.8.10.2-win32-x86.exe)
8. ����������� ���� QNX4.cmake � ���������� <���� ��������� cmake>\share\cmake-2.8\Modules\Platform\
9. ���������� qnx4_build.bat. � ������ ������ ���������� ����� � ���������� install

����������: ��� ������� �� ������ �������� ��������� ��������� ������ � cmake_gen.bat