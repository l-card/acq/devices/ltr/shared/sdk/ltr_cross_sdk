#ifndef CONFIG_H_IN
#define CONFIG_H_IN

#include "ltrapi_config.h"

/* проверка наличия в системе нужных функций и файлов */
/* #undef HAVE_STRNLEN */
#define HAVE_STRING_H 1
#define HAVE_CLOCK_MONOTONIC 1
#define HAVE_SOCKLEN_T 1
/* #undef HAVE_MSG_NOSIGNAL */
/* #undef HAVE_SYS_TIMERS_H */


#ifndef HAVE_STRNLEN
    #include <string.h>
    size_t strnlen(const char *s, size_t maxlen);
#endif


#endif // CONFIG_H_IN
