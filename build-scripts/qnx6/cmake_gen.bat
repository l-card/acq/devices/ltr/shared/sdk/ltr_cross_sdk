rem �������� ����������, ��� ����� ���� �������
set SCRIPT_EXEC_DIR=%~dp0

cd %SCRIPT_EXEC_DIR%

rem --------------  ��������� ������  ---------------------
rem ���� � �������� ����������, ���� ����� ��������� ����������
set DESTDIR=%SCRIPT_EXEC_DIR%install
rem ���� ��������� ������������ �������� ����������
set INSTALL_PREFIX=/usr
rem ��� ������ (Release ��� Debug)
set BUILD_TYPE=Release
rem �������� �� �������
set BUILD_EXAMPLES=OFF

rem ������� ���������� ���������� ������
rmdir /Q /S build
rmdir /Q /S %DESTDIR%

mkdir build
cd build
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=%BUILD_TYPE% -DLTRD_STDOUT_ENABLED=ON -DLTRD_SYSLOG_ENABLED=ON -DCMAKE_TOOLCHAIN_FILE=%SCRIPT_EXEC_DIR%/Toolchain-QNX6.cmake -DCMAKE_INSTALL_PREFIX=%INSTALL_PREFIX% -DLTR_BUILD_EXAMPLES=%BUILD_EXAMPLES% %SCRIPT_EXEC_DIR%/../.. && make && make install

rem ��������� ���������� ����� ������������ (�� ������ ������ ��� cmake)
copy /Y %SCRIPT_EXEC_DIR%\build\ltrapi\include\ltrapi_config.h %SCRIPT_EXEC_DIR%\config\ltrapi
copy /Y %SCRIPT_EXEC_DIR%\build\ltrapi\ltrapi\config.h %SCRIPT_EXEC_DIR%\config\ltrapi
copy /Y %SCRIPT_EXEC_DIR%\build\ltrd\config.h %SCRIPT_EXEC_DIR%\config\ltrd