#!/bin/sh
#получаем директорию, где лежит файл скрипта
SCRIPT_EXEC_DIR=$( cd "$( dirname "$0" )" && pwd )
set -e


# --------------  Параметры сборки  ---------------------
# путь к корневой директории, куда будут сохранены результаты
export DESTDIR=${SCRIPT_EXEC_DIR}/install
# путь установки отностиельно корневой директории
INSTALL_PREFIX=/usr
# тип сборки (Release или Debug)
BUILD_TYPE=Release
#собирать ли примеры
BUILD_EXAMPLES=OFF


# удаляем результаты предыдущей сборки
rm -rf build
rm -rf ${DESTDIR}

cd ${SCRIPT_EXEC_DIR}
mkdir build
cd build
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=${BUILD_TYPE} -DLTRD_STDOUT_ENABLED=ON \
  -DLTRD_SYSLOG_ENABLED=ON -DCMAKE_TOOLCHAIN_FILE=${SCRIPT_EXEC_DIR}/Toolchain-QNX6.cmake \
  -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} -DLTR_BUILD_EXAMPLES=${BUILD_EXAMPLES} \
  ${SCRIPT_EXEC_DIR}/../.. && make && make install

#сохраняем полученные файлы конфигурации (на случай сборки без cmake)
cp ${SCRIPT_EXEC_DIR}/build/ltrapi/include/ltrapi_config.h ${SCRIPT_EXEC_DIR}/config/ltrapi
cp ${SCRIPT_EXEC_DIR}/build/ltrapi/ltrapi/config.h ${SCRIPT_EXEC_DIR}/config/ltrapi
cp ${SCRIPT_EXEC_DIR}/build/ltrd/config.h ${SCRIPT_EXEC_DIR}/config/ltrd